package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity
	implements ConnectionCallbacks,OnConnectionFailedListener, LocationListener {
	
	GoogleMap map;
	Location currentLocation;
	LocationClient locClient;
	LocationRequest locRequest;

	int color = Color.GREEN;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = 
				(MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		//map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		locClient = new LocationClient(this, this, this);
		
		MarkerOptions m = new MarkerOptions();
		m.position(new LatLng(0,0));
		map.addMarker(m);
		
		//PolylineOptions p = new PolylineOptions();
		//p.add(new LatLng(0, 0));
		//p.add(new LatLng(13.75, 100.4667));
		//p.width(10);
		//p.color(Color.BLUE);
		//map.addPolyline(p);
		
		
		
	}
	
	protected void onStart() {
		super.onStart();
		locClient.connect();
	}
	
	protected void onStop() {
		locClient.disconnect();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		currentLocation = locClient.getLastLocation();

		locRequest = LocationRequest.create();
		locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locRequest.setInterval(5*1000);
		locRequest.setFastestInterval(1*1000);
		locClient.requestLocationUpdates(locRequest, this);
	}

	@Override
	public void onDisconnected() {
		
	}

	@Override
	public void onLocationChanged(Location l) {
	//	if (currentLocation.distanceTo(l) > 5) {
//			currentLocation = l;
			
		//	MarkerOptions m = new MarkerOptions();
			//m.position(new LatLng(l.getLatitude(), l.getLongitude()));
			//map.addMarker(m);
	//	}
		
		
		if (currentLocation.distanceTo(l) > 0.1) {

			
		PolylineOptions p = new PolylineOptions();
		p.add(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
		p.add(new LatLng(l.getLatitude(), l.getLongitude()));
		currentLocation = l;
		
		p.width(10);
		p.color(color);
		map.addPolyline(p);
		
		}
		
		
		
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId())
		{
		
		case R.id.Mark:
		{
			
			
			MarkerOptions m = new MarkerOptions();
			m.position(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
			map.addMarker(m);
	
			
		}
				
			break;
			
		case R.id.RandColor:
		{
			Random r = new Random();
			
			color = Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255));
			break;
	
			
		}
				
			
			
			
			
			
		
		
		
		
		
		
		
		}
	
		
		return super.onOptionsItemSelected(item);
	}
	
	
}
